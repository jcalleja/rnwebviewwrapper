import React, { Component } from 'react';
import {
  WebView,
  StyleSheet,
  ActivityIndicator,
  View,
} from 'react-native';

export default class App extends Component<{}> {
  renderLoading = () => {
    return (
      <View  style={styles.container}>
        <ActivityIndicator size="large" color="#0000ff" />
      </View>
    );
  }

  render() {
    // return this.renderLoading();
    return (
      <WebView
        source={{uri: 'http://192.168.0.3:3000'}}
        startInLoadingState
        renderLoading={this.renderLoading}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
